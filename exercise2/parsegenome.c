#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <filename>\n", argv[0]);
        return 1;
    }

    
    FILE *file = fopen(argv[1], "r");
    if (file == NULL) {
        perror("Error opening file");
        return 1;
    }

    char line[102];  
    int line_count = 0;  
    int char_count[4] = {0};  

    
    while (fgets(line, sizeof(line), file) != NULL) {
     
        if (strlen(line) != 101) {
            fprintf(stderr, "Error: Line %d does not have 100 characters.\n", line_count + 1);
            fclose(file);
            return 1;
        }

       
        for (int i = 0; i < 100; i++) {
            char current_char = line[i];
            if (current_char == 'A') {
                char_count[0]++;
            } else if (current_char == 'C') {
                char_count[1]++;
            } else if (current_char == 'G') {
                char_count[2]++;
            } else if (current_char == 'T') {
                char_count[3]++;
            } else {
                fprintf(stderr, "Error: Line %d contains an invalid character.\n", line_count + 1);
                fclose(file);
                return 1;
            }
        }

        line_count++;
    }

  
    fclose(file);

 
    if (line_count != 500) {
        fprintf(stderr, "Error: The file does not have 500 lines.\n");
        return 1;
    }

   
    printf("Character counts:\n");
    printf("A: %d\n", char_count[0]);
    printf("C: %d\n", char_count[1]);
    printf("G: %d\n", char_count[2]);
    printf("T: %d\n", char_count[3]);

    return 0;
}


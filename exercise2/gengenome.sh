#!/bin/bash

dna_bases=("A" "C" "G" "T")

generate_sequence() {
    local sequence=""
    local length="$1"
    for ((i = 0; i < length; i++)); do
        
        local random_base=${dna_bases[$RANDOM % 4]}
        sequence="${sequence}${random_base}"
    done
    echo "$sequence"
}


for ((i = 0; i < 500; i++)); do
    sequence=$(generate_sequence 100)
    echo "$sequence"
done

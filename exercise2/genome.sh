#!/bin/bash

if [ $# -ne 2 ]; then
    echo "Usage: $0 <filename> <search_string>"
    exit 1
fi

filename="$1"
search_string="$2"

count=$(grep -zo "$search_string.*$search_string" "$filename" | wc -l)

echo "The string '$search_string' appears $count times in the file '$filename'."


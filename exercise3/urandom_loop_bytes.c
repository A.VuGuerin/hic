#include <stdio.h>


int main()
{
    __int8_t byte;
    while (byte != 42)
    {
        FILE *file = fopen("/dev/urandom", "r");
        byte = fgetc(file);
        printf("%i %u %x\n", byte, byte, byte);

    }
    
    return 0;

}
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

int main() {
    int urandom_fd = open("/dev/urandom", O_RDONLY);
    if (urandom_fd == -1) {
        perror("Error opening /dev/urandom");
        return 1;
    }

    uint16_t value;

    while (1) {
        ssize_t bytes_read = read(urandom_fd, &value, sizeof(value));

        if (bytes_read == -1) {
            perror("Error reading from /dev/urandom");
            break;
        }

        value &= 0xFFFF;

        printf("%04" PRIX16 "\n", value);

        if (value == 0x002A) {
            break;
        }
    }

    close(urandom_fd);
    return 0;
}
